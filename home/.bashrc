#================================== COLORS =================
# ls colored
alias ls='ls --color=auto'
eval `dircolors -b`

# grep colored
export GREP_COLOR="1;33"
alias grep='grep --color=auto'

# alias for pacman-color
export PACMAN=pacman-color

# color scheme for tty: mirodark - https://github.com/djjcast/mirodark
# https://bbs.archlinux.org/viewtopic.php?pid=559492#p559492
if [[ "$TERM" = "linux" ]]
then
    echo -en "\033]P0000000" # black
    echo -en "\033]P85e5e5e"
    echo -en "\033]P18a2f58" # red
    echo -en "\033]P9cf4f88"
    echo -en "\033]P2287373" # green
    echo -en "\033]Pa53a6a6"
    echo -en "\033]P3914e89" # yellow
    echo -en "\033]Pbbf85cc"
    echo -en "\033]P4395573" # blue
    echo -en "\033]Pc4779b3"
    echo -en "\033]P55e468c" # magenta
    echo -en "\033]Pd7f62b3"
    echo -en "\033]P62b7694" # cyan
    echo -en "\033]Pe47959e"
    echo -en "\033]P7899ca1" # white
    echo -en "\033]Pfc0c0c0"
    clear
fi

#================================== ENVIRONMENT VARS =======
#readline configuration
export INPUTRC=$XDG_CONFIG_HOME/readline/inputrc

#make bash history ignore whites and duplicates
export HISTCONTROL=ignoreboth

export TERM="xterm-256color"
export EDITOR="vim"
export SVN_MERGE="meld"
export PATH=$PATH:$HOME/.local/bin:$HOME/.local/bin/Jess70p1/bin
export BROWSER="exo-open --launch WebBrowser"
export MOZ_PLUGIN_PATH=$MOZ_PLUGIN_PATH:$HOME/.mozilla/plugins
export R600_ENABLE_S3TC=1

# virtualenv python
#export WORKON_HOME=$HOME/.virtualenvs
#source /usr/bin/virtualenvwrapper.sh 
#export PROJECT_HOME=$HOME/workspace

#================================== PS1 ====================
# some colors
BLACK="\[\e[0;30m\]"
DGRAY="\[\e[1;30m\]"
BLUE="\[\e[0;34m\]"
LBLUE="\[\e[1;34m\]"
GREEN="\[\e[0;32m\]"
LGREEN="\[\e[1;32m\]"
CYAN="\[\e[0;36m\]"
LCYAN="\[\e[1;36m\]"
RED="\[\e[0;31m\]"
LRED="\[\e[1;31m\]"
PURPLE="\[\e[0;35m\]"
LPURPLE="\[\e[1;35m\]"
BROWN="\[\e[0;33m\]"
YELLOW="\[\e[1;33m\]"
LGRAY="\[\e[0;37m\]"
WHITE="\[\e[1;37m\]"
RESET_COLOR="\[\e[0m\]"

make_ps1() {
    RETURN_VALUE=$?

    # exit status color
    if [[ $RETURN_VALUE != 0 ]]
	then
        RESULT=$LRED:$RETURN_VALUE:
    else
        RESULT=$LGREEN::
    fi

    # root user color
	if [[ $UID == 0 ]]
	then
        USER_COLOR=$RED
    else 
        USER_COLOR=$GREEN
    fi
   
    PS1="$LBLUE[$USER_COLOR\u$BLUE@$LGRAY\h$LBLUE][$WHITE\w$LBLUE]$RESULT $RESET_COLOR"
}

PROMPT_COMMAND=make_ps1
