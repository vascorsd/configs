" This must be first, because it changes other options as side effect
set nocompatible

" Use pathogen to easily modify the runtime path to include all
" plugins under the ~/.vim/bundle directory
"call pathogen#helptags()
"call pathogen#runtime_append_all_bundles()

set tabstop=4     " a tab is four spaces
set autoindent    " always set autoindenting on
set copyindent    " copy the previous indentation on autoindenting
set number        " always show line numbers
set shiftwidth=4  " number of spaces to use for autoindenting
set showmatch     " set show matching parenthesis
set ignorecase    " ignore case when searching
set smartcase     " ignore case if search pattern is all lowercase,
                  "    case-sensitive otherwise
set incsearch
set nobackup
set noswapfile
set expandtab
set cindent
set softtabstop=4

" set filetype stuff to on
filetype on
filetype plugin on
filetype indent on

syntax on
au BufNewFile,BufRead *.jess set filetype=jess

let g:mirodark_disable_color_approximation=1
colorscheme mirodark

map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop> 
inoremap jj <Esc>
